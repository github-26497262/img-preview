import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    port: 4000,
    host: '0.0.0.0',
  },
  build: {
    outputDir: 'dist',
    lib: {
      entry: 'src/packages/index.js',
      name: 'imagePreview',
      fileName: (format) => `image-preview.${format}.js`,
    },
    rollupOptions: {
      external: ['vue'],
      output: {
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
})
