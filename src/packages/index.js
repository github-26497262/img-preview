import ImagePreview from './imagePreview.vue';
import GridAxis from './gridAxis.vue';
import VueKonva from 'vue-konva';

const coms = [ImagePreview, GridAxis]; // 将来如果有其它组件,都可以写到这个数组里
export default {
  install(Vue) {
    Vue.use(VueKonva);
    coms.forEach((com) => {
      Vue.component(com.name, com);
    });
  },
};
